package com.eledevo.hrm.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Response {
    private String status;
    private String message;
    private Object data;

    public Response(String status, String message) {
        this.status = status;
        this.message = message;
    }
}

