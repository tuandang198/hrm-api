package com.eledevo.hrm.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class ResponseError {
    private Boolean status;
    private String message;
    private String errorMessage;
}
