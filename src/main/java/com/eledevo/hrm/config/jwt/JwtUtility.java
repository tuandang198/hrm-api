package com.eledevo.hrm.config.jwt;

import com.eledevo.hrm.utils.JwtConstants;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

@Component
public class JwtUtility implements Serializable {
    private static final Logger logger = LoggerFactory.getLogger(JwtUtility.class);
    private String SECRET_KEY = "SECRET";

    //tao token
    public String generateToken(String email) {
        return Jwts.builder()
                .setSubject(email)
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + (JwtConstants.EXPIRATION_TIME)))
                .signWith(SignatureAlgorithm.HS512, SECRET_KEY)
                .compact();
    }
    //validate token
    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(JwtConstants.SECRET_KEY).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            logger.error("Invalid JWT signature: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token: {}", e.getMessage());
        }catch (ExpiredJwtException e) {
            logger.error("JWT token is expired: {}", e.getMessage());
        }catch (UnsupportedJwtException e) {
            logger.error("JWT token is unsupported: {}", e.getMessage());
        }catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty: {}", e.getMessage());
        }
        return false;
    }

    public String getEmailFromJwtToken(String token) {
        return Jwts.parser().setSigningKey(JwtConstants.SECRET_KEY).parseClaimsJws(token)
                .getBody().getSubject();
    }
}
