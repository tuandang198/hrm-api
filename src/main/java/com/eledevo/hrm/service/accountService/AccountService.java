package com.eledevo.hrm.service.accountService;

import com.eledevo.hrm.model.in.AccountIn;
import com.eledevo.hrm.model.response.Response;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public interface AccountService extends UserDetailsService {
    Response login(AccountIn accountIn);

    Response register(AccountIn accountIn);
}
