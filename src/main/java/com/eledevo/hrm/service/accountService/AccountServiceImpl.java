package com.eledevo.hrm.service.accountService;

import com.eledevo.hrm.config.jwt.JwtUtility;
import com.eledevo.hrm.model.entity.Account;
import com.eledevo.hrm.model.in.AccountIn;
import com.eledevo.hrm.model.response.Response;
import com.eledevo.hrm.repository.Account.AccountRepository;
import com.eledevo.hrm.service.mapper.AccountMapper;
import com.eledevo.hrm.utils.ResponseString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class AccountServiceImpl implements AccountService{
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtUtility jwtUtility;
    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public Response login(AccountIn accountIn) {
        Account account = AccountMapper.mapAccountIn(accountIn);
        Account checkAccount = accountRepository.findAccountByEmail(account.getEmail());
        if (checkAccount == null) {
            return new Response("false",ResponseString.LoginFail.WRONG_EMAIL,"");
        }
        if(!BCrypt.checkpw(account.getPassword(), checkAccount.getPassword())) {
            return new Response("false",ResponseString.LoginFail.WRONG_PASSWORD,"");
        }
//        Authentication authentication = authenticationManager.authenticate(
//                new UsernamePasswordAuthenticationToken(checkAccount.getEmail(),
//                        checkAccount.getPassword())
//        );
//        SecurityContextHolder.getContext().setAuthentication(authentication);
        //jwt ultility
        String jwt = jwtUtility.generateToken(checkAccount.getEmail());
        return new Response("ok", ResponseString.Login.LOGIN_SUCCESS,jwt);
    }



    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Account account = accountRepository.findAccountByEmail(email);
        if (account == null) {
            throw new UsernameNotFoundException("email" + email+
                    " was not found in the database");
        }
        List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();
        GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("ROLE_"
                + account.getRole());
        grantedAuthorityList.add(grantedAuthority);
        return new User(account.getEmail(),account.getPassword(),grantedAuthorityList);
    }
    @Override
    public Response register(AccountIn accountIn) {
        Account  mapAccountIn = AccountMapper.mapAccountIn(accountIn);
        Account findEmail =accountRepository.findAccountByEmail(mapAccountIn.getEmail());
        if (findEmail==null){
         String BCryptPassword =   BCrypt.hashpw(mapAccountIn.getPassword(),BCrypt.gensalt(10));
            Account account = new Account(mapAccountIn.getEmail(),BCryptPassword);
            accountRepository.save(account);
            System.out.println("aaaa");
            return new Response("ok",ResponseString.Register.REGISTER_SUCCESS,account.getEmail());

        }else {
            return new Response("fail",ResponseString.Register.WRONG_EMAIL);
        }
    }
}
