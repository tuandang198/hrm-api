package com.eledevo.hrm.service.mapper;

import com.eledevo.hrm.model.dto.AccountDto;
import com.eledevo.hrm.model.entity.Account;
import com.eledevo.hrm.model.in.AccountIn;

public class AccountMapper {
    public static Account
    mapAccountIn(AccountIn accountIn) {
        Account account = new Account();
        account.setEmail(accountIn.getEmail());
        account.setPassword(accountIn.getPassword());
        return account;
    }

    public static AccountDto mapAccountEnt(Account account) {
        AccountDto accountDto = new AccountDto();
        accountDto.setEmail(account.getEmail());
        accountDto.setPassword(account.getPassword());
        return accountDto;
    }
}
