package com.eledevo.hrm.utils;

public interface ApiConstants {
    String BASE_URL = "/api/v1";
    interface Login{
        String LOGIN = BASE_URL + "/login";

    }
    interface Register{
        String REGISTER =BASE_URL + "/register";

    }
    interface GetOtp{
        String OTP =BASE_URL +  "/get-otp";

    }
}
