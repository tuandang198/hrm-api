package com.eledevo.hrm.utils;

public interface ResponseString {

    interface Login{
        String LOGIN_SUCCESS = "Dang nhap thanh cong.";
    }

    interface LoginFail{
        String WRONG_PASSWORD = "Sai mat khau";
        String WRONG_EMAIL = "Sai email";
    }

    interface Register{
        String WRONG_EMAIL = "Tài khoản đã tồn tại";
        String REGISTER_SUCCESS = "Đăng ký thành công";
    }
    interface InvalidOtp{
        String WRONG_OTP = "Sai ma OTP";
        String EXPIRED_OTP = "Het han ma OTP";
        String EMAIL_NOT_EXIST = "Email khong ton tai";
        String SERVER_ERROR = "Loi server khong the lay ma otp";
    }

    interface InvalidAttempt {
        String BannedAccount = "Ban da dang nhap qua so lan quy dinh, email nay se bi khoa. Vui long lien he voi admin";
        int MAX_WRONG_ATTEMPT = 5;
    }


}
