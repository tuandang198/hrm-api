package com.eledevo.hrm.repository.Employee;

import com.eledevo.hrm.model.entity.Account;
import com.eledevo.hrm.model.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Long> {

}
