package com.eledevo.hrm.repository.Account;

import com.eledevo.hrm.model.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account,Long> {
    Account findAccountByEmail(String email);
}
