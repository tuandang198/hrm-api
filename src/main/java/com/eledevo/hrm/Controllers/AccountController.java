package com.eledevo.hrm.Controllers;

import com.eledevo.hrm.model.in.AccountIn;
import com.eledevo.hrm.service.accountService.AccountService;
import com.eledevo.hrm.utils.ApiConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
public class AccountController {
    @Autowired
    AccountService accountService;

    @PostMapping(value = ApiConstants.Login.LOGIN)
    ResponseEntity<?> loginAccount(@RequestBody AccountIn accountIn) {
        return ResponseEntity.status(HttpStatus.OK).body(accountService.login(accountIn));
    }

    @GetMapping(value = "test")
    ResponseEntity<?> test() {
        return ResponseEntity.status(HttpStatus.OK).body("Vao man");
    }

    @PostMapping(value =ApiConstants.Register.REGISTER)
     ResponseEntity<?>registerAccount(@RequestBody AccountIn accountIn){
        return ResponseEntity.status(HttpStatus.OK).body(accountService.register(accountIn));
    }
}
