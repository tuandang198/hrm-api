package com.eledevo.hrm.Controllers;

import com.eledevo.hrm.utils.ApiConstants;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(ApiConstants.BASE_URL)
@CrossOrigin("*")
public class EmployeeController {

}
