Create database DB_HRM;

USE DB_HRM;

CREATE TABLE customer (
	id_customer int NOT NULL,
	company varchar(255),
	address varchar(255),
	phone varchar(10),
	representative varchar(100)
);

CREATE TABLE business_partner (
	id_business_partner int NOT NULL,
	company varchar(255),
	address varchar(255),
	phone varchar(10),
	representative varchar(100)
);

CREATE TABLE employee_positions (
	id_employee_positions int NOT NULL,
	name varchar(50)
);

CREATE TABLE workplace (
	id_workplace int NOT NULL,
	id_employee_positions int NOT NULL,
	id_business_partner int NOT NULL,
	id_employee bigint not null,
	id_customer int NOT NULL
);

-- Bang nhan vien
create table employee(
	id_employee bigint not null auto_increment,
	personal_info_id bigint not null,
	attendance_check_status tinyint,
	compare_attendance tinyint,
	employee_salary bigint,
	recommendation_number varchar(20),
	recommended_by_number varchar(20),
	current_business_partner int,
	current_customer int,
	employee_note int,
	employee_state int,
	employee_schedule int,
	end_contract_date date,
	modified_date datetime,
	created_date datetime,
	active tinyint,
	PRIMARY KEY (id_employee)
);

-- Bang salary
create table employee_salary(
	employee_salary_id bigint not null auto_increment,
	hard_salary Decimal,
	passive_income Decimal,
	PRIMARY KEY (employee_salary_id)
);

-- Bang thong tin nhan vien
create table personal_info(
	personal_info_id bigint not null auto_increment,
	name varchar(70),
	address varchar(255),
	email varchar(70),
	phone varchar(10),
	date_of_birth date,
	gender tinyint,
	id_card varchar(20),
	years_of_experience tinyint,
	attendance_check_file varchar(255),
	old_attendance_check_file varchar(255),
	contract_file varchar(255),
	cv_file varchar(255),
	modified_date datetime,
	created_date datetime,
	active tinyint,
	PRIMARY KEY (personal_info_id)
);

-- Bang tinh trang nhan vien 
create table employee_state(
	employee_state_id int not null auto_increment,
	name varchar(70),
	modified_date datetime,
	created_date datetime,
	active tinyint,
	PRIMARY KEY (employee_state_id)
);

-- Lich hen
create table employee_schedule(
	employee_schedule_id int not null auto_increment,
	meeting_date datetime,
	created_by varchar(70),
	purpose varchar(70),
	participants varchar(70),
	modified_date datetime,
	created_date datetime,
	active tinyint,
	PRIMARY KEY (employee_schedule_id)
);

-- ghi chu
create table employee_note(
	employee_note_id int not null auto_increment,
	content text,
	modified_date datetime,
	created_date datetime,
	active tinyint,
	PRIMARY KEY (employee_note_id)
);

-- cong nghe
create table company_tech(
	company_tech_id int not null auto_increment,
	name varchar(255),
	modified_date datetime,
	created_date datetime,
	active tinyint,
	PRIMARY KEY (company_tech_id)
);

-- noi nhan vien vs cong nghe
create table employee_tech(
	id int not null auto_increment,
	tech_id int,
	employee_id bigint,
	PRIMARY KEY (id)
);

create table old_business_partner(
	id int not null auto_increment,
	customer_id int,
	business_partner_id int,
	employee_id bigint,
	PRIMARY KEY (id)
);

-- kc 
ALTER TABLE
	customer
ADD
	CONSTRAINT pk_customer PRIMARY KEY (id_customer);

ALTER TABLE
	employee_positions
ADD
	CONSTRAINT pk_employee_positions PRIMARY KEY (id_employee_positions);

ALTER TABLE
	business_partner
ADD
	CONSTRAINT pk_business_partner PRIMARY KEY (id_business_partner);

ALTER TABLE
	workplace
ADD
	CONSTRAINT pk_id_workplace PRIMARY KEY (id_workplace);

-- Employee fk
ALTER TABLE
	employee
ADD
	CONSTRAINT fk_personal_info FOREIGN KEY (personal_info_id) REFERENCES personal_info(personal_info_id);

ALTER TABLE
	employee
ADD
	CONSTRAINT fk_employee_state FOREIGN KEY (employee_state) REFERENCES employee_state(employee_state_id);

ALTER TABLE
	employee
ADD
	CONSTRAINT fk_employee_schedule FOREIGN KEY (employee_schedule) REFERENCES employee_schedule(employee_schedule_id);

ALTER TABLE
	employee
ADD
	CONSTRAINT fk_employee_note FOREIGN KEY (employee_note) REFERENCES employee_note(employee_note_id);

ALTER TABLE
	employee
ADD
	CONSTRAINT fk_employee_salary FOREIGN KEY (employee_salary) REFERENCES employee_salary(employee_salary_id);

ALTER TABLE
	employee
ADD
	CONSTRAINT fk_employee_customer FOREIGN KEY (current_customer) REFERENCES customer(id_customer);

ALTER TABLE
	employee
ADD
	CONSTRAINT fk_employee_business_partner FOREIGN KEY (current_business_partner) REFERENCES business_partner(id_business_partner);

ALTER TABLE
	employee_tech
ADD
	CONSTRAINT fk_tech_employee FOREIGN KEY (tech_id) REFERENCES company_tech(company_tech_id),
ADD
	CONSTRAINT fk_employee_tech FOREIGN KEY (employee_id) REFERENCES employee(id_employee);

ALTER TABLE
	old_business_partner
ADD
	CONSTRAINT fk_old_customer FOREIGN KEY (customer_id) REFERENCES customer(id_customer),
ADD
	CONSTRAINT fk_old_business_partner FOREIGN KEY (business_partner_id) REFERENCES business_partner(id_business_partner),
ADD
	CONSTRAINT fk_employee_old_company FOREIGN KEY (employee_id) REFERENCES employee(id_employee);

ALTER TABLE
	workplace
ADD
	CONSTRAINT fk1_id_employee_positions FOREIGN KEY (id_employee_positions) REFERENCES employee_positions(id_employee_positions);

ALTER TABLE
	workplace
ADD
	CONSTRAINT fk2_id_business_partner FOREIGN KEY (id_business_partner) REFERENCES business_partner(id_business_partner);

ALTER TABLE
	workplace
ADD
	CONSTRAINT fk3_id_customer FOREIGN KEY (id_customer) REFERENCES customer(id_customer);

ALTER TABLE
	workplace
ADD
	CONSTRAINT fk4_id_employee FOREIGN KEY (id_employee) REFERENCES employee(id_employee);

INSERT INTO
	employee_positions
VALUES
	(1, 'BackEnd');

INSERT INTO
	employee_positions
VALUES
	(2, 'FondEnd');

INSERT INTO
	employee_positions
VALUES
	(3, 'Tester');

INSERT INTO
	employee_positions
VALUES
	(4, 'Design');

INSERT INTO
	employee_positions
VALUES
	(5, 'DEV');

--
INSERT INTO
	customer
VALUES
	(
		1,
		'DVT',
		'43 ngách 15/18 ngõ gốc đề',
		'0869989162',
		'Đàm Văn Thịnh'
	);

INSERT INTO
	customer
VALUES
	(
		2,
		'TUAN',
		'99 ngõ 12 phan đình giót',
		'067381080',
		'Tuấn '
	);

